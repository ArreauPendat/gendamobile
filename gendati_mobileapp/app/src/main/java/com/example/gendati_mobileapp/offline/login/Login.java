package com.example.gendati_mobileapp.offline.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.example.gendati_mobileapp.MainActivity;
import com.example.gendati_mobileapp.R;
import com.example.gendati_mobileapp.SharedPreferencesManager;
import com.example.gendati_mobileapp.model.User;
import com.example.gendati_mobileapp.offline.createacc.CreateAccount;
import com.example.gendati_mobileapp.service.user.UserRepositoryService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity {

    private EditText etLogin;
    private EditText etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etLogin = findViewById(R.id.et_login_login);
        etPassword = findViewById(R.id.et_login_password);

    }

    public void goToCreateAccountActivity(View view) {
        Intent intent = new Intent(Login.this, CreateAccount.class);
        startActivity(intent);
    }

    public void logIn(View view) {
        String email = etLogin.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        UserRepositoryService.connect(email, password).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                SharedPreferencesManager.getOurInstance().setToken(response.body().getToken());
                Log.wtf("ATTENTION", SharedPreferencesManager.getOurInstance().getToken());
                goToMainActivity();
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }

    private void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}
