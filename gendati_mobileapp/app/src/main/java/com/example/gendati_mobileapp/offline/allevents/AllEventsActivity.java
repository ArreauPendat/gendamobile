package com.example.gendati_mobileapp.offline.allevents;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.gendati_mobileapp.R;
import com.example.gendati_mobileapp.adapter.EventAdapter;
import com.example.gendati_mobileapp.adapter.PageUserAdapter;
import com.example.gendati_mobileapp.model.Event;
import com.example.gendati_mobileapp.model.PageUser;
import com.example.gendati_mobileapp.offline.allpages.AllPagesActivity;
import com.example.gendati_mobileapp.service.event.EventRepositoryService;
import com.example.gendati_mobileapp.service.pageuser.PageUserRepositoryService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllEventsActivity extends AppCompatActivity {

    private ListView lv_events;
    private List<Event> events;
    private EventAdapter adapter;

    private Button btnRefresh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_events);

        events = new ArrayList<>();
        initAllEvents();
        lv_events = findViewById(R.id.lv_all_events);
        adapter = new EventAdapter(this, R.id.lv_all_events, events);
        lv_events.setAdapter(adapter);

        btnRefresh = findViewById(R.id.btn_all_events_refresh);
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { initAllEvents(); }
        });
    }

    public void initAllEvents() {
        events.clear();
        EventRepositoryService.getAllEvents().enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                events.addAll(response.body());
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                Toast
                        .makeText(AllEventsActivity.this, "Something went wrong during loading. please try again later.", Toast.LENGTH_LONG)
                        .show();
            }
        });
    }

}
