package com.example.gendati_mobileapp.creation.pageuser;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.gendati_mobileapp.MainActivity;
import com.example.gendati_mobileapp.R;
import com.example.gendati_mobileapp.management.pageuser.PageAdminList;
import com.example.gendati_mobileapp.model.PageUser;
import com.example.gendati_mobileapp.service.pageuser.PageUserRepositoryService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PageUserCreation extends AppCompatActivity {

    private EditText etName;
    private Spinner spType;
    private ArrayAdapter spinnerAdapter;
    private EditText etDescription;
    private EditText etFacebook;
    private EditText etTwitter;
    private EditText etWebsite;
    private Button btnValidate;
    private Button btnNewEvent;

    private PageUser pageUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page_modification);

        pageUser = new PageUser();

        initEditTexts();
        initSpinner();
        initButton();

    }

    private void initButton() {
        btnValidate = findViewById(R.id.btn_pagemoderation_validate);
        btnValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatePageUser();
                if(isFilledCorrectly()) {
                    putToDatabase();
                    goToPageAdminList();
                }
                else
                    toastIt("Please fill required fields.");
            }
        });
        btnNewEvent = findViewById(R.id.btn_pagemoderation_createEvent);
        btnNewEvent.setVisibility(View.INVISIBLE);
    }

    private void goToPageAdminList() {
        Intent intent = new Intent(MainActivity.getContext(), PageAdminList.class);
        startActivity(intent);
    }

    private void toastIt(String msg) {
        Toast
                .makeText(this, msg, Toast.LENGTH_LONG)
                .show();
    }

    private boolean isFilledCorrectly() {
        return !pageUser.getNom().trim().equals("")
                && pageUser.getNom() != null
                && !pageUser.getDescription().trim().equals("")
                && pageUser.getDescription() != null
                && pageUser.getType() != null;
    }

    private void putToDatabase() {
        PageUserRepositoryService.post(pageUser).enqueue(new Callback<PageUser>() {
            @Override
            public void onResponse(Call<PageUser> call, Response<PageUser> response) {
                toastIt("Your page has been created.");
            }

            @Override
            public void onFailure(Call<PageUser> call, Throwable t) {
                toastIt("An error occured while creating your page. Try again later.");
            }
        });
    }

    private void updatePageUser() {
        pageUser.setNom(etName.getText().toString());
        pageUser.setType(spType.getSelectedItem().toString());
        pageUser.setDescription(etDescription.getText().toString());
        pageUser.setLienFacebook(etFacebook.getText().toString());
        pageUser.setLienTwitter(etTwitter.getText().toString());
        pageUser.setLienSite(etWebsite.getText().toString());
    }

    private void initSpinner() {
        spType = findViewById(R.id.sp_pagemeoderation_type);
        spinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.page_types, R.layout.support_simple_spinner_dropdown_item);
        spType.setAdapter(spinnerAdapter);
    }

    private void initEditTexts() {
        etName = findViewById(R.id.et_pagemoderation_pagename);
        etDescription = findViewById(R.id.et_pagemoderation_description);
        etFacebook = findViewById(R.id.et_pagemoderation_facebooklink);
        etTwitter = findViewById(R.id.et_pagemoderation_twitterlink);
        etWebsite = findViewById(R.id.et_pagemoderation_websitelink);
    }
}
