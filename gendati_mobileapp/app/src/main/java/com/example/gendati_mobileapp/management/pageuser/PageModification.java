package com.example.gendati_mobileapp.management.pageuser;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.gendati_mobileapp.R;
import com.example.gendati_mobileapp.creation.event.EventCreation;
import com.example.gendati_mobileapp.model.PageUser;
import com.example.gendati_mobileapp.service.pageuser.PageUserRepositoryService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PageModification extends AppCompatActivity {

    private PageUser pageUser = null;

    private EditText et_name;
    private Spinner sp_type;
    private ArrayAdapter<CharSequence> spinnerAdapter;
    private EditText et_description;
    private EditText et_facebook;
    private EditText et_twitter;
    private EditText et_website;
    private Button btnValidate;

    public static final String IDPAGE_PAGEMODIFICATION = "IDPAGE_PAGEMODIFICATION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page_modification);

        initEditTexts();
        initSpinnerTypes();
        initButtonValidate();
        retrievePageInformation();
    }

    private void initButtonValidate() {
        btnValidate = findViewById(R.id.btn_pagemoderation_validate);
        btnValidate.setEnabled(false);
        btnValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatePageUser();
                putToDatabase();
            }
        });
    }

    private void putToDatabase() {
        PageUserRepositoryService.putPage(pageUser).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                toastIt("Your page has been updated successfully.");
                goBackToPageAdminList();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                toastIt("An error occurred while updating your page.");
            }
        });
    }

    private void updatePageUser() {
        pageUser.setNom(et_name.getText().toString());
        pageUser.setType(sp_type.getSelectedItem().toString());
        pageUser.setDescription(et_description.getText().toString());
        pageUser.setLienFacebook(et_facebook.getText().toString());
        pageUser.setLienTwitter(et_twitter.getText().toString());
        pageUser.setLienSite(et_website.getText().toString());
    }

    private void initSpinnerTypes() {
        sp_type = findViewById(R.id.sp_pagemeoderation_type);
        spinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.page_types, R.layout.support_simple_spinner_dropdown_item);
        sp_type.setAdapter(spinnerAdapter);
    }

    private void goBackToPageAdminList() {
        Intent intent = new Intent(this, PageAdminList.class);
        startActivity(intent);
    }

    private void retrievePageInformation() {
        int id = getIntent().getIntExtra(PageAdminList.CODE_PAGEADMINLIST, -1);
        PageUserRepositoryService.queryById(id).enqueue(new Callback<PageUser>() {
            @Override
            public void onResponse(Call<PageUser> call, Response<PageUser> response) {
                setPageUser(response.body());
                if(pageUser == null) {
                    toastIt("Unable to retrieve page information. Try again later.");
                    goBackToPageAdminList();
                } else {
                    populatePageInformation();
                }
            }

            @Override
            public void onFailure(Call<PageUser> call, Throwable t) {

            }
        });
    }

    private void populatePageInformation() {
        et_name.setText(pageUser.getNom());
        et_description.setText(pageUser.getDescription());
        int positionInList = spinnerAdapter.getPosition(pageUser.getType());
        sp_type.setSelection(positionInList);
        if(validateLinkString(pageUser.getLienFacebook()))
            et_facebook.setText(pageUser.getLienFacebook());
        if(validateLinkString(pageUser.getLienTwitter()))
            et_twitter.setText(pageUser.getLienTwitter());
        if(validateLinkString(pageUser.getLienSite()))
            et_website.setText(pageUser.getLienSite());

        btnValidate.setEnabled(true);

    }

    private boolean validateLinkString(String linkString) {
        return linkString != null && !linkString.trim().equals("");
    }

    private void toastIt(String toastText) {
        Toast
                .makeText(this, toastText, Toast.LENGTH_LONG)
                .show();
    }

    private void initEditTexts() {
        et_name = findViewById(R.id.et_pagemoderation_pagename);
        et_description = findViewById(R.id.et_pagemoderation_description);
        et_facebook = findViewById(R.id.et_pagemoderation_facebooklink);
        et_twitter = findViewById(R.id.et_pagemoderation_twitterlink);
        et_website = findViewById(R.id.et_pagemoderation_websitelink);
    }

    public void setPageUser(PageUser pageUser) {
        this.pageUser = pageUser;
    }

    public void goToCreateEvent(View view) {
        Intent intent = new Intent(this, EventCreation.class);
        intent.putExtra(IDPAGE_PAGEMODIFICATION, pageUser.getId());
        startActivity(intent);
    }
}
