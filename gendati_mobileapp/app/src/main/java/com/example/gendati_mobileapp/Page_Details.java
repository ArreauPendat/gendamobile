package com.example.gendati_mobileapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import android.widget.TextView;
import android.widget.Toast;

import com.example.gendati_mobileapp.model.PageUser;
import com.example.gendati_mobileapp.service.pageuser.PageUserRepositoryService;

import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.gendati_mobileapp.global.allpages.global_all_pages;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Page_Details extends AppCompatActivity {

    private List<PageUser> pages;
    private PageUser pageUser = null;

    private TextView tvName;
    private TextView tvType;
    private TextView tvDescription;

    private Button btnFollow;
    private Button btnUnfollow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page__details);

        pages = new ArrayList<>();

        initTextViewsAndButtons();
        initPageInfos();
        checkIfPageFollowed();
    }

    private void checkIfPageFollowed() {
        pages.clear();
        PageUserRepositoryService.queryExceptUser().enqueue(new Callback<List<PageUser>>() {
            @Override
            public void onResponse(Call<List<PageUser>> call, Response<List<PageUser>> response) {
                pages.addAll(response.body());
                if (pages.contains(pageUser)) {
                    setFollowVisible();
                }
                else{
                    setUnfollowVisible();
                }
            }

            @Override
            public void onFailure(Call<List<PageUser>> call, Throwable t) {
                toastIt("Something went wrong during loading. please try again later.");
            }
        });
    }

    private void initPageInfos() {
        int id = getIntent().getIntExtra(global_all_pages.PAGE_CLICKED, -1);
        PageUserRepositoryService.queryById(id).enqueue(new Callback<PageUser>() {
            @Override
            public void onResponse(Call<PageUser> call, Response<PageUser> response) {
                setPageUser(response.body());
                if(pageUser == null) {
                    toastIt("Unable to retrieve page information. Try again later.");
                    goBackToGlobalAllPages();
                } else {
                    setPageInformations();
                }
            }

            @Override
            public void onFailure(Call<PageUser> call, Throwable t) {
                Log.wtf("Fail","fail");
            }
        });
    }

    public void followPage(View view) {
        pageUser.setUserToken(SharedPreferencesManager.getOurInstance().getToken());
        PageUserRepositoryService.follow(pageUser).enqueue(new Callback<PageUser>() {
            @Override
            public void onResponse(Call<PageUser> call, Response<PageUser> response) {
                setUnfollowVisible();
                Log.wtf("ok",response.toString());
            }

            @Override
            public void onFailure(Call<PageUser> call, Throwable t) {
                setUnfollowVisible();
                Log.wtf("exception",t.toString());
            }
        });
    }

    public void unfollowPage(View view) {
        PageUserRepositoryService.unfollow(pageUser.getId(),SharedPreferencesManager.getOurInstance().getToken()).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                setFollowVisible();
                Log.wtf("ok",response.toString());
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                setFollowVisible();
                Log.wtf("exception",t.toString());
            }
        });
    }

    private void setPageInformations() {
        tvName.setText(pageUser.getNom());
        tvDescription.setText(pageUser.getDescription());
        tvType.setText(pageUser.getType());
    }

    private void initTextViewsAndButtons(){
        tvName = findViewById(R.id.tv_page_name);
        tvType = findViewById(R.id.tv_type);
        tvDescription = findViewById(R.id.tv_description_body);

        btnFollow = findViewById(R.id.btn_follow);
        btnUnfollow = findViewById(R.id.btn_unfollow);
    }

    private void goBackToGlobalAllPages() {
        Intent intent = new Intent(this, global_all_pages.class);
        startActivity(intent);
    }

    private void toastIt(String toastText) {
        Toast
                .makeText(this, toastText, Toast.LENGTH_LONG)
                .show();
    }

    private void setFollowVisible(){
        btnUnfollow.setVisibility(View.INVISIBLE);
        btnFollow.setVisibility(View.VISIBLE);
    }

    private void setUnfollowVisible(){
        btnUnfollow.setVisibility(View.VISIBLE);
        btnFollow.setVisibility(View.INVISIBLE);
    }

    private void setPageUser(PageUser pageUser) {
        this.pageUser = pageUser;
    }

}
