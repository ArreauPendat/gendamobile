package com.example.gendati_mobileapp.repositories;

import com.example.gendati_mobileapp.model.PageUser;
import com.example.gendati_mobileapp.service.Configuration;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface PageUserRepository {

    @GET(Configuration.URL_PAGE)
    Call<List<PageUser>> query();

    @GET(Configuration.URL_PAGE+"/1&{token}")
    Call<List<PageUser>> queryExceptUser(@Path("token") String token);

    @GET(Configuration.URL_PAGE+"/2&{token}")
    Call<List<PageUser>> queryFromUser(@Path("token") String token);

    @GET(Configuration.URL_PAGE+"/3&{token}")
    Call<List<PageUser>> queryModeration(@Path("token") String token);

    @POST(Configuration.URL_SUIT)
    Call<PageUser> follow(@Body PageUser page);

    @DELETE(Configuration.URL_SUIT + "/{idPage}&{token}")
    Call<Void> unfollow(@Path("idPage") int idPage,@Path("token") String token);

    @GET(Configuration.URL_PAGE+"/{idPage}")
    Call<PageUser> queryById(@Path("idPage") int id);

    @PUT(Configuration.URL_PAGE)
    Call<Void> putPage(@Body PageUser page);

    @POST(Configuration.URL_PAGE)
    Call<PageUser> post(@Body PageUser page);
}
