package com.example.gendati_mobileapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

public class PageUser implements Parcelable {

    private int id;
    private String nom;
    private String description;
    private String type;
    private String lienFacebook;
    private String lienTwitter;
    private String lienSite;
    private String userToken;

    public PageUser(){};

    public PageUser(int id, String nom, String description, String type, String lienFacebook, String lienTwitter, String lienSite) {
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.type = type;
        this.lienFacebook = lienFacebook;
        this.lienTwitter = lienTwitter;
        this.lienSite = lienSite;
    }

    public PageUser(String nomPage, String description, String typePage, String linkFacebook, String linkTwitter, String linkWebsite) {
        this.nom = nomPage;
        this.description = description;
        this.type = typePage;
        this.lienFacebook = linkFacebook;
        this.lienTwitter = linkTwitter;
        this.lienSite = linkWebsite;
    }

    protected PageUser(Parcel in) {
        nom = in.readString();
        description = in.readString();
        type = in.readString();
        lienFacebook = in.readString();
        lienTwitter = in.readString();
        lienSite = in.readString();
    }

    public static final Creator<PageUser> CREATOR = new Creator<PageUser>() {
        @Override
        public PageUser createFromParcel(Parcel in) {
            return new PageUser(in);
        }

        @Override
        public PageUser[] newArray(int size) {
            return new PageUser[size];
        }
    };

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLienFacebook() {
        return lienFacebook;
    }

    public void setLienFacebook(String lienFacebook) {
        this.lienFacebook = lienFacebook;
    }

    public String getLienTwitter() {
        return lienTwitter;
    }

    public void setLienTwitter(String lienTwitter) {
        this.lienTwitter = lienTwitter;
    }

    public String getLienSite() {
        return lienSite;
    }

    public void setLienSite(String lienSite) {
        this.lienSite = lienSite;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nom);
        dest.writeString(description);
        dest.writeString(type);
        dest.writeString(lienFacebook);
        dest.writeString(lienTwitter);
        dest.writeString(lienSite);
    }

    @Override
    public String toString() {
        return "PageUser{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", description='" + description + '\'' +
                ", type='" + type + '\'' +
                ", lienFacebook='" + lienFacebook + '\'' +
                ", lienTwitter='" + lienTwitter + '\'' +
                ", lienSite='" + lienSite + '\'' +
                ", userToken='" + userToken + '\'' +
                '}';
    }

    public PageUser clone() {
        return new PageUser(this.nom, this.description, this.type, this.lienFacebook, this.lienTwitter, this.lienSite);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PageUser pageUser = (PageUser) o;
        return Objects.equals(nom, pageUser.nom) &&
                Objects.equals(type, pageUser.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom,type);
    }
}



