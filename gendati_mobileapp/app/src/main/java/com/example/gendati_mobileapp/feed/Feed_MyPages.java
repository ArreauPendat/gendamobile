package com.example.gendati_mobileapp.feed;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.gendati_mobileapp.Page_Details;
import com.example.gendati_mobileapp.R;
import com.example.gendati_mobileapp.adapter.PageUserAdapter;
import com.example.gendati_mobileapp.model.PageUser;
import com.example.gendati_mobileapp.service.pageuser.PageUserRepositoryService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Feed_MyPages extends AppCompatActivity {

    private ListView listpages;
    private List<PageUser> pages;
    private PageUserAdapter adapter;

    public static final String PAGE_CLICKED = "PAGE_CLICKED";

    private Button btnRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed__my_pages);

        pages = new ArrayList<>();
        initAllPages();

        this.listpages = findViewById(R.id.feed_listpages);
        adapter = createAdapter();

        listpages.setAdapter(adapter);
        listpages.setClickable(true);

        btnRefresh = findViewById(R.id.btn_allpages_refresh);
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { initAllPages(); }
        });

    }

    public void initAllPages() {
        pages.clear();
        PageUserRepositoryService.queryFromUser().enqueue(new Callback<List<PageUser>>() {
            @Override
            public void onResponse(Call<List<PageUser>> call, Response<List<PageUser>> response) {
                pages.addAll(response.body());
                adapter.notifyDataSetChanged();
                listpages.setClickable(true);
            }

            @Override
            public void onFailure(Call<List<PageUser>> call, Throwable t) {
                Toast
                        .makeText(Feed_MyPages.this, "Something went wrong during loading. please try again later.", Toast.LENGTH_LONG)
                        .show();
            }
        });
    }

    public PageUserAdapter createAdapter() {
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToPageDetails(v.getId());
            }
        };
        View.OnClickListener onClickListenerFacebook = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                int id = v.getMinimumWidth();
                for (int i = 0; i < pages.size(); i++) {
                    if (pages.get(i).getId()==id) {
                        redirectToURL(pages.get(i).getLienFacebook());
                    }
                }
            }
        };
        View.OnClickListener onClickListenerTwitter = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                int id = v.getMinimumWidth();
                for (int i = 0; i < pages.size(); i++) {
                    if (pages.get(i).getId()==id) {
                        redirectToURL(pages.get(i).getLienTwitter());
                    }
                }
            }
        };
        View.OnClickListener onClickListenerWebsite = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                int id = v.getMinimumWidth();
                for (int i = 0; i < pages.size(); i++) {
                    if (pages.get(i).getId()==id) {
                        redirectToURL(pages.get(i).getLienSite());
                    }
                }
            }
        };
        PageUserAdapter pageUserAdapter = new PageUserAdapter(this, R.id.feed_listpages, pages);
        pageUserAdapter.setOnClickListener(onClickListener);

        pageUserAdapter.setOnClickListenerFacebook(onClickListenerFacebook);
        pageUserAdapter.setOnClickListenerTwitter(onClickListenerTwitter);
        pageUserAdapter.setOnClickListenerWebsite(onClickListenerWebsite);
        return pageUserAdapter;
    }

    public void goToPageDetails(int id) {
        Intent intent = new Intent(this, Page_Details.class);
        intent.putExtra(PAGE_CLICKED, id);
        startActivity(intent);
    }

    private void redirectToURL(String url)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }
}
