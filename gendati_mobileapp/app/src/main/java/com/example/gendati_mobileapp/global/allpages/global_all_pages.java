package com.example.gendati_mobileapp.global.allpages;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.gendati_mobileapp.Page_Details;
import com.example.gendati_mobileapp.R;
import com.example.gendati_mobileapp.adapter.PageUserAdapter;
import com.example.gendati_mobileapp.model.PageUser;
import com.example.gendati_mobileapp.service.pageuser.PageUserRepositoryService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class global_all_pages extends AppCompatActivity{

    private ListView lv_pages;
    private List<PageUser> pages;
    private PageUserAdapter adapter;

    private Button btnRefresh;

    public static final String PAGE_CLICKED = "PAGE_CLICKED";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_global_all_pages);

        pages = new ArrayList<>();
        initAllPages();
        lv_pages = findViewById(R.id.lv_global_allpages);
        adapter = createAdapter();

        lv_pages.setAdapter(adapter);
        lv_pages.setClickable(true);

        btnRefresh = findViewById(R.id.btn_global_allpages_refresh);
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { initAllPages(); }
        });
    }

    private void initAllPages() {
        pages.clear();
        PageUserRepositoryService.queryExceptUser().enqueue(new Callback<List<PageUser>>() {
            @Override
            public void onResponse(Call<List<PageUser>> call, Response<List<PageUser>> response) {
                pages.addAll(response.body());
                adapter.notifyDataSetChanged();
                lv_pages.setClickable(true);
            }

            @Override
            public void onFailure(Call<List<PageUser>> call, Throwable t) {
                Toast
                        .makeText(global_all_pages.this, "Something went wrong during loading. please try again later.", Toast.LENGTH_LONG)
                        .show();
            }
        });
    }

    private PageUserAdapter createAdapter() {
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToPageDetails(v.getId());
            }
        };
        View.OnClickListener onClickListenerFacebook = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                int id = v.getMinimumWidth();
                for (int i = 0; i < pages.size(); i++) {
                    if (pages.get(i).getId()==id) {
                        redirectToURL(pages.get(i).getLienFacebook());
                    }
                }
            }
        };
        View.OnClickListener onClickListenerTwitter = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                int id = v.getMinimumWidth();
                for (int i = 0; i < pages.size(); i++) {
                    if (pages.get(i).getId()==id) {
                        redirectToURL(pages.get(i).getLienTwitter());
                    }
                }
            }
        };
        View.OnClickListener onClickListenerWebsite = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                int id = v.getMinimumWidth();
                for (int i = 0; i < pages.size(); i++) {
                    if (pages.get(i).getId()==id) {
                        redirectToURL(pages.get(i).getLienSite());
                    }
                }
            }
        };

        PageUserAdapter pageUserAdapter = new PageUserAdapter(this, R.id.lv_global_allpages, pages);
        pageUserAdapter.setOnClickListener(onClickListener);

        pageUserAdapter.setOnClickListenerFacebook(onClickListenerFacebook);
        pageUserAdapter.setOnClickListenerTwitter(onClickListenerTwitter);
        pageUserAdapter.setOnClickListenerWebsite(onClickListenerWebsite);

        return pageUserAdapter;
    }

    private void goToPageDetails(int id) {
        Intent intent = new Intent(this, Page_Details.class);
        intent.putExtra(PAGE_CLICKED, id);
        startActivity(intent);
    }

    private void redirectToURL(String url)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }
}
