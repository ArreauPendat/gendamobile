package com.example.gendati_mobileapp.service;

public interface Configuration {

    final String URL_API = "https://gendaeventplanner.azurewebsites.net/api/";
    final String URL_PAGE = "page";
    final String URL_SUIT = "suit";
    final String URL_EVENT = "evenement";
    final String URL_USER = "utilisateur";
    final String URL_PARTICIPE = "participe";
}
