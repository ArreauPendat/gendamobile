package com.example.gendati_mobileapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.gendati_mobileapp.R;
import com.example.gendati_mobileapp.management.pageuser.PageAdminList;
import com.example.gendati_mobileapp.model.PageUser;

import java.util.List;

import static androidx.core.content.ContextCompat.startActivity;

public class PageUserAdapter extends ArrayAdapter<PageUser> {

    private View.OnClickListener onClickListener;

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    private View.OnClickListener onClickListenerFacebook;

    public void setOnClickListenerFacebook(View.OnClickListener onClickListenerFacebook) {
        this.onClickListenerFacebook = onClickListenerFacebook;
    }

    private View.OnClickListener onClickListenerTwitter;

    public void setOnClickListenerTwitter(View.OnClickListener onClickListenerTwitter) {
        this.onClickListenerTwitter = onClickListenerTwitter;
    }

    private View.OnClickListener onClickListenerWebsite;

    public void setOnClickListenerWebsite(View.OnClickListener onClickListenerWebsite) {
        this.onClickListenerWebsite = onClickListenerWebsite;
    }

    public PageUserAdapter(@NonNull Context context, int resource, @NonNull List<PageUser> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;

        if(v == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            v = inflater.inflate(R.layout.item_pageuser, null);
        }

        TextView name = v.findViewById(R.id.tv_pagename);
        TextView type = v.findViewById(R.id.tv_pagetype);

        Button btnFacebook = v.findViewById(R.id.btn_pageuser_facebook);
        Button btnTwitter = v.findViewById(R.id.btn_pageuser_twitter);
        Button btnWebsite = v.findViewById(R.id.btn_pageuser_website);

        PageUser page = getItem(position);

        name.setText(page.getNom());
        type.setText(page.getType());

        v.setId(page.getId());

        if(page.getLienFacebook() == null || page.getLienFacebook().trim().equals("")) {
            btnFacebook.setVisibility(View.INVISIBLE);
        } else {
            btnFacebook.setOnClickListener(onClickListenerFacebook);
            // 21:26 - J'arrivais pas à passer l'id, j'ai paniqué désolé
            btnFacebook.setMinimumWidth(page.getId());
        }
        if(page.getLienTwitter() == null || page.getLienTwitter().trim().equals("")) {
            btnTwitter.setVisibility(View.INVISIBLE);
        } else {
            btnTwitter.setOnClickListener(onClickListenerTwitter);
            // 21:26 - J'arrivais pas à passer l'id, j'ai paniqué désolé
            btnTwitter.setMinimumWidth(page.getId());
        }
        if(page.getLienSite() == null || page.getLienSite().trim().equals("")) {
            btnWebsite.setVisibility(View.INVISIBLE);
        } else {
            btnWebsite.setOnClickListener(onClickListenerWebsite);
            // 21:26 - J'arrivais pas à passer l'id, j'ai paniqué désolé
            btnWebsite.setMinimumWidth(page.getId());
        }

        v.setOnClickListener(onClickListener);
        return v;

    }
}
