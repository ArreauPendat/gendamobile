package com.example.gendati_mobileapp.service.pageuser;

import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;

import com.example.gendati_mobileapp.model.PageUser;
import com.example.gendati_mobileapp.repositories.PageUserRepository;
import com.example.gendati_mobileapp.service.Configuration;
import com.example.gendati_mobileapp.SharedPreferencesManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PageUserRepositoryService extends AppCompatActivity {

    private PageUserRepository repository;
    private static final PageUserRepositoryService ourInstance = new PageUserRepositoryService();

    private PageUserRepositoryService() { init(); }

    private void init() {
        repository = new Retrofit.Builder()
        .baseUrl(Configuration.URL_API)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(PageUserRepository.class);
    }

    public static Call<List<PageUser>> queryAll() {
        return ourInstance.repository.query();
    }
    public static Call<List<PageUser>> queryModeration() {
        return ourInstance.repository.queryModeration(SharedPreferencesManager.getOurInstance().getToken());
    }
    public static Call<List<PageUser>> queryExceptUser() {
        return ourInstance.repository.queryExceptUser(SharedPreferencesManager.getOurInstance().getToken());
    }
    public static Call<List<PageUser>> queryFromUser(){
        return ourInstance.repository.queryFromUser(SharedPreferencesManager.getOurInstance().getToken());
    }
    public static Call<PageUser> follow(PageUser page) {
        return ourInstance.repository.follow(page);
    }
    public static Call<Void> unfollow(int idPage, String token){
        return ourInstance.repository.unfollow(idPage,token);
    }
    public static Call<PageUser> queryById(int id) {
        return ourInstance.repository.queryById(id);
    }

    public static Call<Void> putPage(PageUser page) { return ourInstance.repository.putPage(page); }

    public static Call<PageUser> post(PageUser page) {
        page.setUserToken(SharedPreferencesManager.getOurInstance().getToken());
        return ourInstance.repository.post(page);
    }

}
