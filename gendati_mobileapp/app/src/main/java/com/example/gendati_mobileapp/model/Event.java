package com.example.gendati_mobileapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.Objects;

public class Event implements Parcelable, Serializable {

    private int idEvent;
    private String nomEvent;
    private String descriptionEvent;
    private String dateDebEvent;
    private String dateFinEvent;
    private int nbPersMinEvent;
    private int nbPersMaxEvent;
    private int idOrga;
    private String userToken;

    public Event(int idEvent, String nomEvent, String descriptionEvent, String dateDebEvent, String dateFinEvent, int nbPersMinEvent, int nbPersMaxEvent, int idOrga) {
        this.idEvent = idEvent;
        this.nomEvent = nomEvent;
        this.descriptionEvent = descriptionEvent;
        this.dateDebEvent = dateDebEvent;
        this.dateFinEvent = dateFinEvent;
        this.nbPersMinEvent = nbPersMinEvent;
        this.nbPersMaxEvent = nbPersMaxEvent;
        this.idOrga = idOrga;
    }

    public Event(String nomEvent, String descriptionEvent, String dateDebEvent, String dateFinEvent, int nbPersMinEvent, int nbPersMaxEvent, int idOrga) {
        this.nomEvent = nomEvent;
        this.descriptionEvent = descriptionEvent;
        this.dateDebEvent = dateDebEvent;
        this.dateFinEvent = dateFinEvent;
        this.nbPersMinEvent = nbPersMinEvent;
        this.nbPersMaxEvent = nbPersMaxEvent;
        this.idOrga = idOrga;
    }

    public Event() { }

    protected Event(Parcel in) {
        idEvent = in.readInt();
        nomEvent = in.readString();
        descriptionEvent = in.readString();
        dateDebEvent = in.readString();
        dateFinEvent = in.readString();
        nbPersMinEvent = in.readInt();
        nbPersMaxEvent = in.readInt();
        idOrga = in.readInt();
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    @Override
    public String toString() {
        return "Event{" +
                "idEvent=" + idEvent +
                ", nomEvent='" + nomEvent + '\'' +
                ", descriptionEvent='" + descriptionEvent + '\'' +
                ", dateDebEvent=" + dateDebEvent +
                ", dateFinEvent=" + dateFinEvent +
                ", nbPersMinEvent=" + nbPersMinEvent +
                ", nbPersMaxEvent=" + nbPersMaxEvent +
                ", idOrga=" + idOrga +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(idEvent);
        dest.writeString(nomEvent);
        dest.writeString(descriptionEvent);
        dest.writeString(dateDebEvent);
        dest.writeString(dateFinEvent);
        dest.writeInt(nbPersMinEvent);
        dest.writeInt(nbPersMaxEvent);
        dest.writeInt(idOrga);
    }

    public int getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(int idEvent) {
        this.idEvent = idEvent;
    }

    public String getNomEvent() {
        return nomEvent;
    }

    public void setNomEvent(String nomEvent) {
        this.nomEvent = nomEvent;
    }

    public String getDescriptionEvent() {
        return descriptionEvent;
    }

    public void setDescriptionEvent(String descriptionEvent) {
        this.descriptionEvent = descriptionEvent;
    }

    public String getDateDebEvent() {
        return dateDebEvent;
    }

    public void setDateDebEvent(String dateDebEvent) {
        this.dateDebEvent = dateDebEvent;
    }

    public String getDateFinEvent() {
        return dateFinEvent;
    }

    public void setDateFinEvent(String dateFinEvent) {
        this.dateFinEvent = dateFinEvent;
    }

    public int getNbPersMinEvent() {
        return nbPersMinEvent;
    }

    public void setNbPersMinEvent(int nbPersMinEvent) {
        this.nbPersMinEvent = nbPersMinEvent;
    }

    public int getNbPersMaxEvent() {
        return nbPersMaxEvent;
    }

    public void setNbPersMaxEvent(int nbPersMaxEvent) {
        this.nbPersMaxEvent = nbPersMaxEvent;
    }

    public int getIdOrga() {
        return idOrga;
    }

    public void setIdOrga(int idOrga) {
        this.idOrga = idOrga;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return Objects.equals(nomEvent, event.nomEvent) &&
                Objects.equals(dateDebEvent, event.dateDebEvent) &&
                Objects.equals(dateFinEvent, event.dateFinEvent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nomEvent, dateDebEvent, dateFinEvent);
    }
}
