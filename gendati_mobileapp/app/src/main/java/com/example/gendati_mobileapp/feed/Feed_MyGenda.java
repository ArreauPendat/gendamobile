package com.example.gendati_mobileapp.feed;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gendati_mobileapp.R;
import com.example.gendati_mobileapp.adapter.EventAdapter;
import com.example.gendati_mobileapp.model.Event;
import com.example.gendati_mobileapp.service.event.EventRepositoryService;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.example.gendati_mobileapp.model.Event;
import com.example.gendati_mobileapp.service.event.EventRepositoryService;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Feed_MyGenda extends AppCompatActivity{

    private List<Event> events;
    private List<EventDay> eventDays;
    private TextView textView;
    private com.applandeo.materialcalendarview.CalendarView calendarView;
    private int day;
    private int month;
    private int year;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed__my_genda);

        calendarView = findViewById(R.id.calendarView);
        textView = findViewById(R.id.tv_eventinfo);

        eventDays = new ArrayList<>();
        events = new ArrayList<>();

        initEvents();
    }

     public void initEvents() {
        events.clear();
        EventRepositoryService.getEventsJoined().enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                events.addAll(response.body());
                Log.wtf("rrr",""+events.size());
                setColorEvent();
            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                Toast
                        .makeText(Feed_MyGenda.this, "Something went wrong during loading. please try again later.", Toast.LENGTH_LONG)
                        .show();
            }
        });
    }

    public void initCalendar(){
        calendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                Calendar tmp = eventDay.getCalendar();
                day = tmp.get(Calendar.DAY_OF_MONTH);
                month = tmp.get(Calendar.MONTH) + 1;
                Log.wtf("aaa",""+month);
                year = tmp.get(Calendar.YEAR);
                setCalendarEvent();
            }
        });
    }

    public void setColorEvent(){

        for(Event e : events){
            int dayEvent = Integer.parseInt(e.getDateDebEvent().substring(8,10));
            int monthEvent = Integer.parseInt(e.getDateDebEvent().substring(5,7));
            int yearEvent = Integer.parseInt(e.getDateDebEvent().substring(0,4));

            int dayEventF = Integer.parseInt(e.getDateFinEvent().substring(8,10));
            int monthEventF = Integer.parseInt(e.getDateFinEvent().substring(5,7));
            int yearEventF = Integer.parseInt(e.getDateFinEvent().substring(0,4));


            if(dayEvent == dayEvent && monthEvent == monthEventF && yearEvent == yearEventF)
            {
                Calendar calendar = Calendar.getInstance();
                calendar.set(yearEvent,monthEvent-1,dayEvent);
                eventDays.add(new EventDay(calendar, R.drawable.sample_icon_sameday));
            }
            else {
                Calendar calendar1 = Calendar.getInstance();
                calendar1.set(yearEvent, monthEvent - 1, dayEvent);
                eventDays.add(new EventDay(calendar1, R.drawable.sample_icon_start));

                Calendar calendar2 = Calendar.getInstance();
                calendar2.set(yearEventF, monthEventF - 1, dayEventF);
                eventDays.add(new EventDay(calendar2, R.drawable.sample_icon_end));
            }
        }
        calendarView.setEvents(eventDays);
        initCalendar();
    }

    public void setCalendarEvent()
    {
        String txt = "";
        for(Event e : events){
            Log.wtf("aaa",e.getDateDebEvent());

            int dayEvent = Integer.parseInt(e.getDateDebEvent().substring(8,10));
            int dayEventF = Integer.parseInt(e.getDateFinEvent().substring(8,10));

            Log.wtf("aaa",""+dayEvent + " " + dayEventF);

            int monthEvent = Integer.parseInt(e.getDateDebEvent().substring(5,7));
            int monthEventF = Integer.parseInt(e.getDateFinEvent().substring(5,7));

            Log.wtf("aaa",""+monthEvent + " " + monthEventF);

            int yearEvent = Integer.parseInt(e.getDateDebEvent().substring(0,4));
            int yearEventF = Integer.parseInt(e.getDateFinEvent().substring(0,4));

            Log.wtf("aaa",""+yearEvent + " " + yearEventF);

            if(day == dayEvent || day == dayEventF){
                Log.wtf("aaa" , "day OK");
                if(month == monthEvent || month == monthEventF){
                    Log.wtf("aaa" , "month OK");
                    if(year == yearEvent && year == yearEventF){
                        Log.wtf("aaa" , "year OK");

                        txt +=
                                "" + e.getNomEvent() + "\n"
                                + e.getDateDebEvent() + " à " + e.getDateFinEvent() + "\n"
                                + e.getNbPersMinEvent() + " à " + e.getNbPersMaxEvent() + " personnes \n"
                                + e.getDescriptionEvent() +"\n\n" ;
                    }
                }
            }

        }
        textView.setText(txt);
    }
}
