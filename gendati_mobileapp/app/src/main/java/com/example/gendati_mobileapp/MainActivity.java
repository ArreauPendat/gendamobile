package com.example.gendati_mobileapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.gendati_mobileapp.global.allevents.GlobalAllEvents;
import com.example.gendati_mobileapp.creation.pageuser.PageUserCreation;
import com.example.gendati_mobileapp.feed.Feed_MyEvents;
import com.example.gendati_mobileapp.feed.Feed_MyGenda;
import com.example.gendati_mobileapp.feed.Feed_MyPages;
import com.example.gendati_mobileapp.global.allpages.global_all_pages;
import com.example.gendati_mobileapp.global.trends.GlobalTrends;
import com.example.gendati_mobileapp.management.event.EventAdminList;
import com.example.gendati_mobileapp.management.pageuser.PageAdminList;
import com.example.gendati_mobileapp.offline.OfflineMainActivity;

import java.io.Serializable;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    public static final String EVENT_CLICKED_NAME = "EVENT_CLICKED_NAME";

    View background;

    public static Context getContext() {
        return applicationContext;
    }

    private static Context applicationContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        applicationContext = MainActivity.this;
        background = findViewById(R.id.background_main_activity);

//        SharedPreferences preferences = getSharedPreferences(SharedPreferencesManager.GENDA_PREFERENCES, MODE_PRIVATE);
//        String token = preferences.getString(SharedPreferencesManager.TOKEN_STRINGNAME, null);
//        if(token == null) {
//            preferences.edit().putString(SharedPreferencesManager.TOKEN_STRINGNAME, "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJJZGVudGlmaWVyIjoiMzciLCJuYmYiOjE1NzYxNDE4NDcsImV4cCI6MTU3Njc0NjY0NywiaWF0IjoxNTc2MTQxODQ3fQ.BRvXf_A4lFdokhSM2uBdG6NfjQ1etpge5FXscBDN-QI").commit();
//            token = preferences.getString(SharedPreferencesManager.TOKEN_STRINGNAME, null);
//        }
        //SharedPreferencesManager.getOurInstance().setToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJJZGVudGlmaWVyIjoiMzUiLCJuYmYiOjE1NzYxNDkwNjYsImV4cCI6MTU3Njc1Mzg2NiwiaWF0IjoxNTc2MTQ5MDY2fQ.jDKqsIKGk1NMC0yms6z5GJsLxx51VgTMLebBWiI3gps");
        String token = SharedPreferencesManager.getOurInstance().getToken();
        if(token == null || token.trim().equals(""))
            logout(null);
    }

    public void changerFond(View view) {
        background.setBackgroundColor(Color.rgb(generateRGBParameter(), generateRGBParameter(), generateRGBParameter()));
    }

    private int generateRGBParameter() {
        return new Random().nextInt(256);
    }

    public void goToOfflineMainActivity(View view) {
        Intent intent = new Intent(this, OfflineMainActivity.class);
        startActivity(intent);
    }

    public void goToGlobalAllPages(View view) {
        Intent intent = new Intent(this, global_all_pages.class);
        startActivity(intent);
    }

    public void goToGlobalAllEvents(View view) {
        Intent intent = new Intent(this, GlobalAllEvents.class);
        startActivity(intent);
    }

    public void goToGlobalTrends(View view) {
        Intent intent = new Intent(this, GlobalTrends.class);
        startActivity(intent);
    }

    public void goToFeedMyPages(View view) {
        Intent intent = new Intent(this, Feed_MyPages.class);
        startActivity(intent);
    }

    public void goToFeedMyEvents(View view) {
        Intent intent = new Intent(this, Feed_MyEvents.class);
        startActivity(intent);
    }

    public void goToFeedMyGenda(View view) {
        Intent intent = new Intent(this, Feed_MyGenda.class);
        startActivity(intent);
    }

    public void goToManagementAllPages(View view) {
        Intent intent = new Intent(this, PageAdminList.class);
        startActivity(intent);
    }

    public void goToManagementAllEvents(View view) {
        Intent intent = new Intent(this, EventAdminList.class);
        startActivity(intent);
    }

    public void goToManagementNewPage(View view) {
        Intent intent = new Intent(this, PageUserCreation.class);
        startActivity(intent);
    }

    public void logout(View view) {
        SharedPreferencesManager.getOurInstance().resetToken();
        Intent intent = new Intent(MainActivity.this, OfflineMainActivity.class);
        startActivity(intent);
    }
}
