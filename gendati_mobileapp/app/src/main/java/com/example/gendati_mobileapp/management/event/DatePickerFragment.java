package com.example.gendati_mobileapp.management.event;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import java.util.Calendar;

public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    private int dates[] = null;
    private TextView display;

    public DatePickerFragment(TextView display) {
        this.display = display;
        dates = new int[3];
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Do something with the date chosen by the user
        setDates(year, month, day);
        putDatesInTextView();
    }

    private void putDatesInTextView() {
        display.setText(String.format("%04d", dates[0])+"-"+String.format("%02d", dates[1])+"-"+String.format("%02d", dates[2]));
    }

    public int[] getDates() {
        return dates;
    }

    public void setDates(int year, int month, int day) {
        dates[0] = year;
        dates[1] = month + 1;
        dates[2] = day;
    }

    public void setDates(String dateInString) {
        String[] dataSplitted = dateInString.split("-");
        dates[0] = Integer.parseInt(dataSplitted[0]);
        dates[1] = Integer.parseInt(dataSplitted[1]);
        dates[2] = Integer.parseInt(dataSplitted[2]);
        putDatesInTextView();
    }
}
