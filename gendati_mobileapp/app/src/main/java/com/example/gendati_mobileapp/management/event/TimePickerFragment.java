package com.example.gendati_mobileapp.management.event;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.fragment.app.DialogFragment;

import java.util.Calendar;

public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    private int[] times = null;
    private TextView display;

    public TimePickerFragment(TextView display) {
        this.display = display;
        times = new int[2];
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        // Do something with the time chosen by the user
        setTimes(hourOfDay, minute);
        putTimesInTextView();
    }

    private void putTimesInTextView() {
        display.setText(String.format("%02d", times[0]) + ":" + String.format("%02d", times[1]));
    }

    public int[] getTimes() {
        return times;
    }

    public void setTimes(int hour, int minute) {
        times[0] = hour;
        times[1] = minute;
    }

    public void setTimes(String timeInString) {
        String dataSplitted[] = timeInString.split(":");
        times[0] = Integer.parseInt(dataSplitted[0]);
        times[1] = Integer.parseInt(dataSplitted[1]);
        putTimesInTextView();
    }
}
