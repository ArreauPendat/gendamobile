package com.example.gendati_mobileapp.global.trends;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.example.gendati_mobileapp.Event_Details;
import com.example.gendati_mobileapp.R;
import com.example.gendati_mobileapp.adapter.EventAdapter;
import com.example.gendati_mobileapp.model.Event;
import com.example.gendati_mobileapp.service.event.EventRepositoryService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.gendati_mobileapp.global.allevents.GlobalAllEvents.EVENT_CLICKED;

public class GlobalTrends extends AppCompatActivity {

    private ListView lv_events;
    private List<Event> events;
    private EventAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_global_trends);

        events = new ArrayList<>();

        lv_events = findViewById(R.id.lv_global_trends);
        createAdapter();

        lv_events.setAdapter(adapter);
        lv_events.setClickable(true);
        initAllEvents();
    }
    private void initAllEvents() {
        events.clear();
        EventRepositoryService.getTrends().enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                events.addAll(response.body());
                adapter.notifyDataSetChanged();
                lv_events.setClickable(true);
                Log.wtf("Ok",response.body().toString());
            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                Log.wtf("fail",t.toString());
            }
        });
    }

    private void createAdapter() {
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToEventDetails(v.getId());
            }
        };
        adapter = new EventAdapter(this, R.id.lv_global_trends, events);
        adapter.setOnClickListener(onClickListener);
    }

    private Event sendEventWithId(int id) {
        for(Event ev : events) {
            if(ev.getIdEvent() == id) return ev;
        }
        return null;
    }

    private void goToEventDetails(int id) {
        Intent intent = new Intent(this, Event_Details.class);
        intent.putExtra(EVENT_CLICKED, (Serializable) sendEventWithId(id));
        startActivity(intent);
    }

    public void refreshEvents(View view) {
        initAllEvents();
    }

}
