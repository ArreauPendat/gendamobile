package com.example.gendati_mobileapp.management.pageuser;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;

import com.example.gendati_mobileapp.MainActivity;
import com.example.gendati_mobileapp.R;
import com.example.gendati_mobileapp.adapter.PageUserAdapter;
import com.example.gendati_mobileapp.model.PageUser;
import com.example.gendati_mobileapp.service.pageuser.PageUserRepositoryService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PageAdminList extends AppCompatActivity {

    private ListView lv_pages;
    private List<PageUser> pages;
    private PageUserAdapter adapter;

    private Button btnRefresh;

    public static final String CODE_PAGEADMINLIST = "CODE_PAGEADMINLIST";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page_admin_list);

        pages = new ArrayList<>();
        initPagesList();
        lv_pages = findViewById(R.id.lv_management_pagelist);
        adapter = createAdapter();
        lv_pages.setAdapter(adapter);
        lv_pages.setClickable(true);

        btnRefresh = findViewById(R.id.btn_adminpagelist_refresh);
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { initPagesList(); }
        });

    }

    public PageUserAdapter createAdapter() {
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToModerationPanel(v.getId());
            }
        };
        View.OnClickListener onClickListenerFacebook = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                int id = v.getMinimumWidth();
                for (int i = 0; i < pages.size(); i++) {
                    if (pages.get(i).getId()==id) {
                        redirectToURL(pages.get(i).getLienFacebook());
                    }
                }
            }
        };
        View.OnClickListener onClickListenerTwitter = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                int id = v.getMinimumWidth();
                for (int i = 0; i < pages.size(); i++) {
                    if (pages.get(i).getId()==id) {
                        redirectToURL(pages.get(i).getLienTwitter());
                    }
                }
            }
        };
        View.OnClickListener onClickListenerWebsite = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                int id = v.getMinimumWidth();
                for (int i = 0; i < pages.size(); i++) {
                    if (pages.get(i).getId()==id) {
                        redirectToURL(pages.get(i).getLienSite());
                    }
                }
            }
        };
        PageUserAdapter pageUserAdapter = new PageUserAdapter(this, R.id.lv_management_pagelist, pages);
        pageUserAdapter.setOnClickListener(onClickListener);

        pageUserAdapter.setOnClickListenerFacebook(onClickListenerFacebook);
        pageUserAdapter.setOnClickListenerTwitter(onClickListenerTwitter);
        pageUserAdapter.setOnClickListenerWebsite(onClickListenerWebsite);
        return pageUserAdapter;
    }

    public void initPagesList() {
        pages.clear();
        PageUserRepositoryService.queryModeration().enqueue(new Callback<List<PageUser>>() {
            @Override
            public void onResponse(Call<List<PageUser>> call, Response<List<PageUser>> response) {
                pages.addAll(response.body());
                Collections.sort(pages, new Comparator<PageUser>() {
                    @Override
                    public int compare(PageUser o1, PageUser o2) {
                        return  o1.getNom().compareToIgnoreCase(o2.getNom());
                    }
                });
                adapter.notifyDataSetChanged(); //refresh the listview
                lv_pages.setClickable(true);
            }

            @Override
            public void onFailure(Call<List<PageUser>> call, Throwable t) {
                Toast
                        .makeText(PageAdminList.this, "Something went wrong during loading. please tyr again later.", Toast.LENGTH_LONG)
                        .show();
            }
        });
    }

    public void goToModerationPanel(int id) {
        Intent intent = new Intent(this, PageModification.class);
        intent.putExtra(CODE_PAGEADMINLIST, id);
        startActivity(intent);
    }

    private void redirectToURL(String url)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }
}
