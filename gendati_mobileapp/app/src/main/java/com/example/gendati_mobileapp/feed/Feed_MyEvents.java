package com.example.gendati_mobileapp.feed;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.gendati_mobileapp.Event_Details;
import com.example.gendati_mobileapp.R;
import com.example.gendati_mobileapp.adapter.EventAdapter;
import com.example.gendati_mobileapp.model.Event;
import com.example.gendati_mobileapp.service.event.EventRepositoryService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.gendati_mobileapp.global.allevents.GlobalAllEvents.EVENT_CLICKED;

public class Feed_MyEvents extends AppCompatActivity {

    private ListView listevent;
    private List<Event> events;
    private EventAdapter adapter;

    private Button btnRefresh;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed__my_events);

        events = new ArrayList<>();

        listevent = findViewById(R.id.feed_eventlist);

        createAdapter();

        listevent.setAdapter(adapter);
        listevent.setClickable(true);
        initEvents();
        btnRefresh = findViewById(R.id.btn_allEvent_refresh);
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { initEvents(); }
        });


    }

    public void initEvents() {
        events.clear();
        EventRepositoryService.getEventsJoined().enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                events.addAll(response.body());
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                Toast
                        .makeText(Feed_MyEvents.this, "Something went wrong during loading. please try again later.", Toast.LENGTH_LONG)
                        .show();
            }
        });
    }

    private void createAdapter() {
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToEventDetails(v.getId());
            }
        };
        adapter = new EventAdapter(this, R.id.feed_eventlist, events);
        adapter.setOnClickListener(onClickListener);
    }

    private Event sendEventWithId(int id) {
        for(Event ev : events) {
            if(ev.getIdEvent() == id) return ev;
        }
        return null;
    }

    private void goToEventDetails(int id) {
        Intent intent = new Intent(this, Event_Details.class);
        intent.putExtra(EVENT_CLICKED, (Serializable) sendEventWithId(id));
        startActivity(intent);
    }
}
