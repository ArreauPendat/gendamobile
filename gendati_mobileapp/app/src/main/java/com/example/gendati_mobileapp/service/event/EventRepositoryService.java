package com.example.gendati_mobileapp.service.event;

import android.content.SharedPreferences;

import com.example.gendati_mobileapp.SharedPreferencesManager;
import com.example.gendati_mobileapp.model.Event;
import com.example.gendati_mobileapp.model.PageUser;
import com.example.gendati_mobileapp.repositories.EventRepository;
import com.example.gendati_mobileapp.service.Configuration;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EventRepositoryService {

    private EventRepository repository;
    private static EventRepositoryService ourInstance = new EventRepositoryService();

    private EventRepositoryService() {
        init();
    }

    private void init() {
        repository = new Retrofit.Builder()
                .baseUrl(Configuration.URL_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(EventRepository.class);
    }

    public static Call<List<Event>> queryModeration() { return ourInstance.repository.getModeratedEvent(SharedPreferencesManager.getOurInstance().getToken()); }
    public static Call<Event> join(Event event) {
        return ourInstance.repository.join(event);
    }
    public static Call<Void> leave(int idEvent, String token){
        return ourInstance.repository.leave(idEvent,token);
    }

    public static Call<List<Event>> queryExceptUser() { return ourInstance.repository.getExceptUser(SharedPreferencesManager.getOurInstance().getToken());}

    public static Call<Void> putEvent(Event event) { return ourInstance.repository.putEvent(event); }

    public static  Call<Void> delete(int idEvent) { return ourInstance.repository.deleteEvent(idEvent); }

    public static Call<Event> post(Event event) { return ourInstance.repository.post(event); }

    public static Call<List<Event>> getAllEvents(){ return ourInstance.repository.getAllEvents("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJJZGVudGlmaWVyIjoiNTgiLCJuYmYiOjE1NzY0MTYwMTAsImV4cCI6MTU3NzAyMDgxMCwiaWF0IjoxNTc2NDE2MDEwfQ.2jBz0MdJEET9aH-aAfWd22VeMygq1qpMqOY6DU8sx9k");}

    public static Call<List<Event>> getEventsJoined(){ return ourInstance.repository.getEventsJoined(SharedPreferencesManager.getOurInstance().getToken());}

    public static Call<List<Event>> getTrends(){ return ourInstance.repository.getTrends();}
}
