package com.example.gendati_mobileapp.repositories;

import com.example.gendati_mobileapp.model.Event;
import com.example.gendati_mobileapp.service.Configuration;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface EventRepository {

    @GET(Configuration.URL_EVENT+"/3&{token}")
    Call<List<Event>> getModeratedEvent(@Path("token") String token);

    @POST(Configuration.URL_PARTICIPE)
    Call<Event> join(@Body Event event);

    @DELETE(Configuration.URL_PARTICIPE + "/{idEvent}&{token}")
    Call<Void> leave(@Path("idEvent") int idEvent,@Path("token") String token);

    @GET(Configuration.URL_EVENT+"/1&{token}")
    Call<List<Event>> getExceptUser(@Path("token") String token);

    @GET(Configuration.URL_EVENT+"/1&{tokenOfflineUser}")
    Call<List<Event>> getAllEvents(@Path("tokenOfflineUser") String token);

    @GET(Configuration.URL_EVENT+"/2&{token}")
    Call<List<Event>> getEventsJoined(@Path("token") String token);
    
    @PUT(Configuration.URL_EVENT)
    Call<Void> putEvent(@Body Event event);

    @DELETE(Configuration.URL_EVENT +"/{id}")
    Call<Void> deleteEvent(@Path("id") int idEvent);

    @POST(Configuration.URL_EVENT)
    Call<Event> post(@Body Event event);

    @GET(Configuration.URL_EVENT)
    Call<List<Event>> getTrends();
}
