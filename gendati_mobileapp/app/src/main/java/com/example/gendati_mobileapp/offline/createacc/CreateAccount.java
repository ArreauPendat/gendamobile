package com.example.gendati_mobileapp.offline.createacc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.gendati_mobileapp.MainActivity;
import com.example.gendati_mobileapp.R;
import com.example.gendati_mobileapp.SharedPreferencesManager;
import com.example.gendati_mobileapp.model.User;
import com.example.gendati_mobileapp.repositories.UserRepository;
import com.example.gendati_mobileapp.service.user.UserRepositoryService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateAccount extends AppCompatActivity {

    private User user = new User();

    private EditText etName;
    private EditText etFirstName;
    private EditText etEmail;
    private EditText etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        etName = findViewById(R.id.et_accountcreation_name);
        etFirstName = findViewById(R.id.et_accountcreation_firstname);
        etEmail = findViewById(R.id.et_accountcreation_email);
        etPassword = findViewById(R.id.et_accountcreation_password);

    }



    public void createAccount(View view) {
        if (!buildUser()) return;
        UserRepositoryService.createAccount(user).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                SharedPreferencesManager.getOurInstance().setToken(response.body().getToken());
                goToMainActivity();
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }

    private void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private boolean buildUser() {
        if(etName.getText().toString() == null || etName.getText().toString().trim().equals("")) return false;
        else user.setNom(etName.getText().toString().trim());
        if(etFirstName.getText().toString() == null || etFirstName.getText().toString().trim().equals("")) return false;
        else user.setPrenom(etFirstName.getText().toString().trim());
        if(etEmail.getText().toString() == null || etEmail.getText().toString().trim().equals("")) return false;
        else user.setEmail(etEmail.getText().toString().trim());
        if(etPassword.getText().toString() == null || etPassword.getText().toString().trim().equals("")) return false;
        else user.setPassword(etPassword.getText().toString().trim());
        return true;
    }

}
