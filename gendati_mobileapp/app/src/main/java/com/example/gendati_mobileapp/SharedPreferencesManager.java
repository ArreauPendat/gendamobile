package com.example.gendati_mobileapp;

import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public class SharedPreferencesManager extends AppCompatActivity {


    public static final String TOKEN_STRINGNAME = "GendaToken";
    public static final String GENDA_PREFERENCES = "GendaApp";
    private static SharedPreferencesManager ourInstance;
    private SharedPreferences preferences;

    private SharedPreferencesManager() {
        preferences = MainActivity.getContext().getSharedPreferences("GendaApp", MODE_PRIVATE);
    }

    public static SharedPreferencesManager getOurInstance() {
        if(ourInstance == null)
            ourInstance = new SharedPreferencesManager();
        return ourInstance;
    }

    public String getToken() {
        return preferences.getString(TOKEN_STRINGNAME, null);
    }

    public String setToken(String newToken) {
        preferences.edit().putString(TOKEN_STRINGNAME, newToken).commit();
        return newToken;
    }

    public boolean resetToken() {
        preferences.edit().remove(TOKEN_STRINGNAME).commit();
        return getToken() == null;
    }


}
