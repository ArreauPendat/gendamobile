package com.example.gendati_mobileapp.repositories;

import com.example.gendati_mobileapp.model.User;
import com.example.gendati_mobileapp.service.Configuration;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface UserRepository {

    @GET(Configuration.URL_USER+"/{email}&{password}")
    Call<User> connect(@Path("email")String email, @Path("password")String password);

    @POST(Configuration.URL_USER)
    Call<User> create(@Body User user);
}
