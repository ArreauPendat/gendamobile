package com.example.gendati_mobileapp.management.pageuser;

import android.os.AsyncTask;

import com.example.gendati_mobileapp.model.PageUser;

import java.lang.ref.WeakReference;
import java.util.List;

import javax.security.auth.callback.Callback;

public class PageAdminListAsyncTask extends AsyncTask<List<PageUser>, PageUser, Void> {

    private WeakReference<Callback> callback;

    public PageAdminListAsyncTask(Callback callback) {
        this.callback = new WeakReference<>(callback);
    }

    public interface Callback {
        void addToPageList(PageUser page);
        void onFinished();
    }

    @Override
    protected Void doInBackground(List<PageUser>... lists) {
        for (int i = 0; i < lists[0].size(); i++) {
            publishProgress(lists[0].get(i));
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(PageUser... values) {
        this.callback.get().addToPageList(values[0]);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        this.callback.get().onFinished();
    }
}
