package com.example.gendati_mobileapp.offline;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.gendati_mobileapp.R;
import com.example.gendati_mobileapp.offline.allevents.AllEventsActivity;
import com.example.gendati_mobileapp.offline.allpages.AllPagesActivity;
import com.example.gendati_mobileapp.offline.createacc.CreateAccount;
import com.example.gendati_mobileapp.offline.login.Login;

public class OfflineMainActivity extends AppCompatActivity {

    private Button allPages;
    private Button allEvents;

    private Button logIn;
    private Button createAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offline_main_activity);
        allPages = findViewById(R.id.btn_offline_allpages);
        allEvents = findViewById(R.id.btn_offline_allevents);
        logIn = findViewById(R.id.btn_offline_login);
        createAccount = findViewById(R.id.btn_offline_createacc);
    }

    public void goToAllPagesActivity(View view) {
        Intent intent = new Intent(this, AllPagesActivity.class);
        startActivity(intent);
    }

    public void goToAllEventsActivity(View view) {
        Intent intent = new Intent(this, AllEventsActivity.class);
        startActivity(intent);
    }

    public void goToLoginActivity(View view) {
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
    }

    public void goToCreateAccountActivity(View view) {
        Intent intent = new Intent(this, CreateAccount.class);
        startActivity(intent);
    }
}
