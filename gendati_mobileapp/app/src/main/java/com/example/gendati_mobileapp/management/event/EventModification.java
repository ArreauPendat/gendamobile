package com.example.gendati_mobileapp.management.event;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gendati_mobileapp.R;
import com.example.gendati_mobileapp.model.Event;
import com.example.gendati_mobileapp.service.event.EventRepositoryService;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventModification extends AppCompatActivity {

    private Event event;

    private TextView tvEndingTime;
    private TextView tvEndingDate;
    private TextView tvStartingTime;
    private TextView tvStartingDate;
    private EditText etName;
    private EditText etDescription;
    private EditText etNbMin;
    private EditText etNbMax;

    private TimePickerFragment startingTimePicker;
    private TimePickerFragment endingTimePicker;
    private DatePickerFragment startingDatePicker;
    private DatePickerFragment endingDatePicker;

    private Button btnValidate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_modification);

        retrieveEventFromExtra();

        initElements();
        initPickers();

        changeForEventData();
        changeDatesAndTimes();

        initButton();

    }

    private void initButton() {
        btnValidate = findViewById(R.id.btn_eventmoderation_validate);
        btnValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateEvent();
                putToDatabase();
            }
        });
    }

    private void putToDatabase() {
        EventRepositoryService.putEvent(event).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Toast
                        .makeText(EventModification.this, "Your event has been updated successfully.", Toast.LENGTH_LONG)
                        .show();
                goBackToEventAdminList();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }

    private void goBackToEventAdminList() {
        Intent intent = new Intent(this, EventAdminList.class);
        startActivity(intent);
    }

    private void updateEvent() {
        event.setNomEvent(etName.getText().toString());
        event.setDescriptionEvent(etDescription.getText().toString());
        event.setNbPersMinEvent(Integer.parseInt(etNbMin.getText().toString()));
        event.setNbPersMaxEvent(Integer.parseInt(etNbMax.getText().toString()));
        event.setDateDebEvent(formatStringForDatabase(tvStartingDate, tvStartingTime));
        event.setDateFinEvent(formatStringForDatabase(tvEndingDate, tvEndingTime));
    }

    private String formatStringForDatabase(TextView tvDate, TextView tvTime) {
        return tvDate.getText().toString() + "T" + tvTime.getText().toString() + ":00";
    }

    private void changeForEventData() {
        etName.setText(event.getNomEvent());
        etDescription.setText(event.getDescriptionEvent());
        etNbMin.setText("" + event.getNbPersMinEvent());
        etNbMax.setText("" + event.getNbPersMaxEvent());
    }

    private void changeDatesAndTimes() {
        String dateStart = getDataFromString(event.getDateDebEvent(),0);
        String dateEnd = getDataFromString(event.getDateFinEvent(),0);
        String timeStart = getDataFromString(event.getDateDebEvent(), 1);
        String timeEnd = getDataFromString(event.getDateFinEvent(), 1);

        startingDatePicker.setDates(dateStart);
        startingTimePicker.setTimes(timeStart);
        endingDatePicker.setDates(dateEnd);
        endingTimePicker.setTimes(timeEnd);

    }

    private String getDataFromString(String stringDate, int index) {
        return stringDate.split("T")[index];
    }

    private void retrieveEventFromExtra() {
        event = new Gson().fromJson(getIntent().getStringExtra(EventAdminList.CODE_EVENTADMINLIST), Event.class);
    }

    private void initElements() {
        tvEndingTime = findViewById(R.id.tv_eventmoderation_endingtime);
        tvEndingDate = findViewById(R.id.tv_eventmoderation_endingdate);
        tvStartingTime = findViewById(R.id.tv_eventmoderation_startingtime);
        tvStartingDate = findViewById(R.id.tv_eventmoderation_startingdate);
        etName = findViewById(R.id.et_eventmoderation_name);
        etDescription = findViewById(R.id.et_eventmoderation_description);
        etNbMin = findViewById(R.id.et_eventmoderation_nbmin);
        etNbMax = findViewById(R.id.et_eventmoderation_nbmax);
    }

    private void initPickers() {
        initListeners();
        startingDatePicker = new DatePickerFragment(tvStartingDate);
        startingTimePicker = new TimePickerFragment(tvStartingTime);
        endingDatePicker = new DatePickerFragment(tvEndingDate);
        endingTimePicker = new TimePickerFragment(tvEndingTime);
    }

    private void initListeners() {
        tvStartingTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showStartingTimePickerDialog(v);
            }
        });
        tvEndingTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEndingTimePickerDialog(v);
            }
        });
        tvStartingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showStartingDatePickerDialog(v);
            }
        });
        tvEndingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEndingDatePickerDialog(v);
            }
        });
    }

    public void showStartingTimePickerDialog(View v) {
        startingTimePicker.show(getSupportFragmentManager(), "timePicker");
    }

    public void showEndingTimePickerDialog(View v) {
        endingTimePicker.show(getSupportFragmentManager(), "timePicker");
    }

    public void showStartingDatePickerDialog(View view) {
        startingDatePicker.show(getSupportFragmentManager(), "datePicker");
    }

    public void showEndingDatePickerDialog(View view) {
        endingDatePicker.show(getSupportFragmentManager(), "datePicker");
    }

    public void deleteEvent(View view) {
        Log.wtf("ATTENTION", event.getIdEvent() + "");
        EventRepositoryService.delete(event.getIdEvent()).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                goBackToEventAdminList();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(EventModification.this, "An error occurred", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
