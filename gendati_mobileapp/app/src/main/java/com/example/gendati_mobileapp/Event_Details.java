package com.example.gendati_mobileapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gendati_mobileapp.global.allevents.GlobalAllEvents;
import com.example.gendati_mobileapp.model.Event;
import com.example.gendati_mobileapp.service.event.EventRepositoryService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.gendati_mobileapp.MainActivity.EVENT_CLICKED_NAME;
import static com.example.gendati_mobileapp.global.allevents.GlobalAllEvents.EVENT_CLICKED;

public class Event_Details extends AppCompatActivity {

    private TextView tvName;
    private TextView tvDateBegin;
    private TextView tvDateEnd;
    private TextView tvNbMin;
    private TextView tvNbMax;
    private TextView tvDescription;

    private Button btnJoin;
    private Button btnLeave;

    private Event event = null;
    private List<Event> events;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event__details);

        events = new ArrayList<>();

        initTextViewsAndButtons();
        initEventInfos();
        checkIfEventJoined();

    }

    private void checkIfEventJoined() {
        events.clear();
        EventRepositoryService.queryExceptUser().enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                events.addAll(response.body());
                if (events.contains(event)) {
                    setJoinVisible();
                }
                else{
                    setLeaveVisible();
                }
            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                toastIt("Something went wrong during loading. please try again later.");
            }
        });
    }

    private void initEventInfos() {
        Event event = (Event) getIntent().getSerializableExtra(EVENT_CLICKED);
        setEvent(event);
        if(event==null)
        {
            toastIt("Unable to retrieve page information. Try again later.");
            goBackToGlobalAllEvents();
        }
        else
        {
            setEventInformations();
        }
    }

    public void joinEvent(View view) {
        event.setUserToken(SharedPreferencesManager.getOurInstance().getToken());
        EventRepositoryService.join(event).enqueue(new Callback<Event>() {
            @Override
            public void onResponse(Call<Event> call, Response<Event> response) {
                setLeaveVisible();
                Log.wtf("ok",response.toString());
            }

            @Override
            public void onFailure(Call<Event> call, Throwable t) {
                setLeaveVisible();
                Log.wtf("exception",t.toString());
            }
        });
    }

    public void leaveEvent(View view) {
        EventRepositoryService.leave(event.getIdEvent(),SharedPreferencesManager.getOurInstance().getToken()).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                setJoinVisible();
                Log.wtf("ok",response.toString());
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                setJoinVisible();
                Log.wtf("exception",t.toString());
            }
        });
    }

    private void initTextViewsAndButtons(){
        tvName = findViewById(R.id.tv_event_name);
        tvDescription = findViewById(R.id.tv_event_description);
        tvDateBegin = findViewById(R.id.tv_event_begin);
        tvDateEnd = findViewById(R.id.tv_event_end);
        tvNbMin = findViewById(R.id.tv_nb_min);
        tvNbMax = findViewById(R.id.tv_nb_max);

        btnJoin = findViewById(R.id.btn_join);
        btnLeave = findViewById(R.id.btn_leave);
    }

    private void setEventInformations() {
        tvName.setText(event.getNomEvent());
        tvDescription.setText(event.getDescriptionEvent());
        tvDateBegin.setText(event.getDateDebEvent());
        tvDateEnd.setText(event.getDateFinEvent());
        tvNbMin.setText(event.getNbPersMinEvent()+"");
        tvNbMax.setText(event.getNbPersMaxEvent()+"");
    }

    private void goBackToGlobalAllEvents() {
        Intent intent = new Intent(this, GlobalAllEvents.class);
        startActivity(intent);
    }

    private void setEvent(Event event) {
        this.event = event;
    }

    private void toastIt(String toastText) {
        Toast
                .makeText(this, toastText, Toast.LENGTH_LONG)
                .show();
    }

    private void setJoinVisible(){
        btnLeave.setVisibility(View.INVISIBLE);
        btnJoin.setVisibility(View.VISIBLE);
    }

    private void setLeaveVisible(){
        btnLeave.setVisibility(View.VISIBLE);
        btnJoin.setVisibility(View.INVISIBLE);
    }
}
