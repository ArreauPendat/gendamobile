package com.example.gendati_mobileapp.creation.event;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.gendati_mobileapp.R;
import com.example.gendati_mobileapp.management.event.DatePickerFragment;
import com.example.gendati_mobileapp.management.event.EventAdminList;
import com.example.gendati_mobileapp.management.event.TimePickerFragment;
import com.example.gendati_mobileapp.management.pageuser.PageModification;
import com.example.gendati_mobileapp.model.Event;
import com.example.gendati_mobileapp.service.event.EventRepositoryService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventCreation extends AppCompatActivity {

    private EditText etName;
    private EditText etDescription;
    private EditText etNbMin;
    private EditText etNbMax;

    private TextView tvStartingDate;
    private TextView tvStartingTime;
    private TextView tvEndingDate;
    private TextView tvEndingTime;
    private TimePickerFragment startingTimePicker;
    private TimePickerFragment endingTimePicker;
    private DatePickerFragment startingDatePicker;
    private DatePickerFragment endingDatePicker;

    private Button btnValidate;
    private Button btnDelete;

    private Event event;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_modification);

        event = new Event();

        initEditText();
        initTextViews();
        initButtons();
        initPickers();


    }

    private void initTextViews() {
        tvEndingTime = findViewById(R.id.tv_eventmoderation_endingtime);
        tvEndingDate = findViewById(R.id.tv_eventmoderation_endingdate);
        tvStartingTime = findViewById(R.id.tv_eventmoderation_startingtime);
        tvStartingDate = findViewById(R.id.tv_eventmoderation_startingdate);
    }

    private void initPickers() {
        initListeners();
        startingDatePicker = new DatePickerFragment(tvStartingDate);
        startingTimePicker = new TimePickerFragment(tvStartingTime);
        endingDatePicker = new DatePickerFragment(tvEndingDate);
        endingTimePicker = new TimePickerFragment(tvEndingTime);
    }

    private void initButtons() {
        btnDelete = findViewById(R.id.btn_admineventlist_delete);
        btnDelete.setVisibility(View.INVISIBLE);
        btnValidate = findViewById(R.id.btn_eventmoderation_validate);
        btnValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateEvent();
                putToDatabase();
            }
        });
    }

    private void putToDatabase() {
        EventRepositoryService.post(event).enqueue(new Callback<Event>() {
            @Override
            public void onResponse(Call<Event> call, Response<Event> response) {
                toastIt("Your event has been created.");
                goToEventList();
            }

            @Override
            public void onFailure(Call<Event> call, Throwable t) {
                toastIt("An error occurred.");
            }
        });
    }

    private void goToEventList() {
        Intent intent = new Intent(this, EventAdminList.class);
        startActivity(intent);
    }

    private void toastIt(String msg) {
        Toast
                .makeText(this, msg, Toast.LENGTH_LONG)
                .show();
    }

    private void initEditText() {
        etName = findViewById(R.id.et_eventmoderation_name);
        etDescription = findViewById(R.id.et_eventmoderation_description);
        etNbMin = findViewById(R.id.et_eventmoderation_nbmin);
        etNbMax = findViewById(R.id.et_eventmoderation_nbmax);
    }

    private void initListeners() {
        tvStartingTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showStartingTimePickerDialog(v);
            }
        });
        tvEndingTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEndingTimePickerDialog(v);
            }
        });
        tvStartingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showStartingDatePickerDialog(v);
            }
        });
        tvEndingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEndingDatePickerDialog(v);
            }
        });
    }

    public void showStartingTimePickerDialog(View v) {
        startingTimePicker.show(getSupportFragmentManager(), "timePicker");
    }

    public void showEndingTimePickerDialog(View v) {
        endingTimePicker.show(getSupportFragmentManager(), "timePicker");
    }

    public void showStartingDatePickerDialog(View view) {
        startingDatePicker.show(getSupportFragmentManager(), "datePicker");
    }

    public void showEndingDatePickerDialog(View view) {
        endingDatePicker.show(getSupportFragmentManager(), "datePicker");
    }

    private void updateEvent() {
        event.setNomEvent(etName.getText().toString());
        event.setDescriptionEvent(etDescription.getText().toString());
        event.setNbPersMinEvent(Integer.parseInt(etNbMin.getText().toString()));
        event.setNbPersMaxEvent(Integer.parseInt(etNbMax.getText().toString()));
        event.setDateDebEvent(formatStringForDatabase(tvStartingDate, tvStartingTime));
        event.setDateFinEvent(formatStringForDatabase(tvEndingDate, tvEndingTime));
        event.setIdOrga(retrieveIdPageFromIntent());
    }

    private int retrieveIdPageFromIntent() {
        return getIntent().getIntExtra(PageModification.IDPAGE_PAGEMODIFICATION, -1);
    }

    private String formatStringForDatabase(TextView tvDate, TextView tvTime) {
        return tvDate.getText().toString() + "T" + tvTime.getText().toString() + ":00";
    }
}
