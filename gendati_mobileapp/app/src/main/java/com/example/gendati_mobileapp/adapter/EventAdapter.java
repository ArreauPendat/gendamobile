package com.example.gendati_mobileapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.gendati_mobileapp.R;
import com.example.gendati_mobileapp.model.Event;

import org.w3c.dom.Text;

import java.util.List;

public class EventAdapter extends ArrayAdapter<Event> {

    private View.OnClickListener onClickListener = null;

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public EventAdapter(@NonNull Context context, int resource, @NonNull List<Event> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;

        if(v == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            v = inflater.inflate(R.layout.item_event, null);
        }

        TextView name = v.findViewById(R.id.tv_eventname);
        TextView startingDate = v.findViewById(R.id.tv_startingdate);
        TextView endingDate = v.findViewById(R.id.tv_endingdate);
        TextView minNumber = v.findViewById(R.id.tv_minnumber);
        TextView maxNumber = v.findViewById(R.id.tv_maxnumber);

        Event event = getItem(position);

        name.setText(event.getNomEvent());
        startingDate.setText(event.getDateDebEvent());
        endingDate.setText(event.getDateFinEvent());

        minNumber.setText("" +event.getNbPersMinEvent());
        maxNumber.setText("" +event.getNbPersMaxEvent());

        v.setId(event.getIdEvent());

        v.setOnClickListener(onClickListener);

        return v;

    }
}
