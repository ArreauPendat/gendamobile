package com.example.gendati_mobileapp.management.event;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.example.gendati_mobileapp.R;
import com.example.gendati_mobileapp.adapter.EventAdapter;
import com.example.gendati_mobileapp.model.Event;
import com.example.gendati_mobileapp.service.event.EventRepositoryService;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventAdminList extends AppCompatActivity {

    private List<Event> events;

    private ListView lv_events;
    private EventAdapter adapter;

    private Button btnRefresh;
    public static final String CODE_EVENTADMINLIST = "CODE_EVENTADMINLIST";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_admin_list);

        events = new ArrayList<>();
        lv_events = findViewById(R.id.lv_management_eventlist);
        createAdapter();
        lv_events.setAdapter(adapter);
        initEventList();

        btnRefresh = findViewById(R.id.btn_admineventlist_refresh);
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initEventList();
                adapter.notifyDataSetChanged();
            }
        });

    }

    private void createAdapter() {
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToModerationPanel(v.getId());
            }
        };
        adapter = new EventAdapter(this, R.id.lv_management_pagelist, events);
        adapter.setOnClickListener(onClickListener);
    }

    private void goToModerationPanel(int id) {
        Intent intent = new Intent(this, EventModification.class);
        intent.putExtra(CODE_EVENTADMINLIST, new Gson().toJson(sendEventWithId(id)));
        startActivity(intent);
    }

    private Event sendEventWithId(int id) {
        for(Event ev : events) {
            if(ev.getIdEvent() == id) return ev;
        }
        return null;
    }

    public void initEventList() {
        events.clear();
        EventRepositoryService.queryModeration().enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                events.addAll(response.body());
                Collections.sort(events, new Comparator<Event>() {
                    @Override
                    public int compare(Event o1, Event o2) {
                        return o1.getNomEvent().compareToIgnoreCase(o2.getNomEvent());
                    }
                });
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
            }
        });
    }
}
