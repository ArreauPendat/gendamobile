package com.example.gendati_mobileapp.service.user;

import com.example.gendati_mobileapp.model.User;
import com.example.gendati_mobileapp.repositories.UserRepository;
import com.example.gendati_mobileapp.service.Configuration;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserRepositoryService {

    private UserRepository repository;
    private static UserRepositoryService ourInstance = new UserRepositoryService();

    public UserRepositoryService() { init(); }

    private void init() {
        repository = new Retrofit.Builder()
                .baseUrl(Configuration.URL_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(UserRepository.class);
    }

    public static Call<User> connect(String email, String password) {
        return ourInstance.repository.connect(email, password);
    }

    public static Call<User> createAccount(User user) {
        return ourInstance.repository.create(user);
    }

}
